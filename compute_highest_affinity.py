# No need to process files and manipulate strings - we will
# pass in lists (of equal length) that correspond to 
# sites views. The first list is the site visited, the second is
# the user who visited the site.

# See the test cases for more details.

def highest_affinity(site_list, user_list, time_list):
  # Returned string pair should be ordered by dictionary order
  # I.e., if the highest affinity pair is "foo" and "bar"
  # return ("bar", "foo"). 
  site_dict = {}
  max_a=-1;
  max_s=[];
  for x in range(0,len(time_list)):
    if site_dict.has_key(site_list[x]):
      site_dict[site_list[x]].add(user_list[x])
    else:
      site_dict[site_list[x]]=set([user_list[x]])
  for x in site_dict.iterkeys():
    for y in site_dict.iterkeys():
      if x==y:
        continue
      else:
        if len(site_dict[x] & site_dict[y])>max_a:
          max_a=len(site_dict[x] & site_dict[y])
          max_s=[x, y]
  max_s.sort()
  return (max_s[0], max_s[1])
def testx1():
  print "test"
def testx2():
  print "test2"
def testx3():
  print "test3"
